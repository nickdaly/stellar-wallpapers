* Stellar Wallpapers

These wallpapers are based on NASA's [[https://solarsystem.nasa.gov/resources/all/?order=pub_date+desc&per_page=50&page=0&search=&condition_1=1%3Ais_in_resource_list&fs=&fc=324&ft=&dp=&category=324][3D resources]], exported at 30 rotations of 12 degrees each with Blender as 1920x1080 PNGs.

As the source works are [[https://www.nasa.gov/multimedia/guidelines/index.html][not under copyright in the US]], neither is this.

* Installation

1. Edit ~wallpaper.xml~ and Find+Replace ~user~ with your username.
2. ~ln -s (your-checkout-location) ~/.stellar~
3. Copy ~stellar.xml~ to ~/usr/share/gnome-background-properties/stellar.xml~.
4. Log out of your Gnome session and then log back in.
5. Open Gnome Settings, select the Background tab, and pick the picture with the planet.

* DIY

Use the below snippets in [[https://orgmode.org/][Org Mode]] to export your own planet pictures.

1. Find+Replace the target planet's name in both code snippets.  Use lower-case to make it easy on yourself later.
2. Run the wallpaper export with ~C-c C-c~.  Save it somewhere permanent.
3. Open ~data/nasa-planet.blend~ in Blender.
4. Import ~~data/planet~ with ~[File] >> [Import] >> [glTF]~.
5. Rename the imported object to be consistent with the target's name in the Blender Export code.
6. Scale the object's X, Y, and Z each to 0.004.
7. Hit F12 to make sure it looks okay.
   - Venus (Surface) :: Change the ~[Material Properties] >> [Specular]~ to 0.
   - Sun :: Scale the object to somewhere around 4 - 10.
   - Saturn :: Open ~exports/saturn/nasa-saturn.blend~ instead.
8. Switch to the Scripting tab and paste the Blender Export code into the console and hit enter twice.
9. Watch the export directory for your new files.
10. Point ~stellar.xml~ ~filename~ to your new ~wallpaper.xml~.
11. Copy ~stellar.xml~ to ~/usr/share/gnome-background-properties/stellar.xml~.

** Wallpaper XML Export

#+name: wallpaper
#+begin_src python :results output file :file wallpaper.xml
  target = "venus"
  frames = 30

  head = """<background>
    <starttime>
      <year>2011</year>
      <month>11</month>
      <day>24</day>
      <hour>0</hour>
      <minute>00</minute>
      <second>00</second>
    </starttime>
  """

  tail = "</background>"

  xml = """  <static>
      <duration>660.0</duration>
      <file>~/.stellar/exports/{0}/{2}/{0}-{1:02d}-{2}.png</file>
    </static>
  """

  print(head)
  for frame in range(frames):
      print(xml.format(target, frame, frames))
  print(tail)
#+end_src

#+RESULTS:
[[file:wallpaper.xml]]

** Blender Export

Blender export!

#+name: blender
#+begin_src python
  from math import pi

  target = "venus"
  filepath = "~/.stellar/exports/{0}/{2}/{0}-{1:02d}-{2}.png"
  planet = bpy.data.objects[target]
  planet.rotation_mode = "XYZ"
  frames = 30

  for frame in range(frames):
      planet.rotation_euler = (0,0, radians(360 * frame / frames))
      bpy.ops.render.render()
      bpy.data.images["Render Result"].save_render(filepath.format(target,frame,frames))
#+end_src
